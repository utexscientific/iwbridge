# IW Bridge

IWBridge is a .NET library to enable 3rd party software to communicate with InspectionWare powered applications.

![Release](https://img.shields.io/badge/release-pre--alpha-blue?style=flat-square) \
[![Website](https://img.shields.io/badge/contact-UTEX_Scientific-yellow?style=flat-square)](https://www.utex.com/webapps/utexhome.nsf/wPages/MSZI-74SS924000?Open)

## Getting Started

>Check out the __IWBridgeSample__ project in the repository for a sample Windows Forms application, it is designed to work with the __TCPServerSample__ InspectionWare workspace.

All communication requires an instance of  `IWServer` object and some object that implements `TransportManager`.

The library ships with a `SocketTransportManager` which can be used to communicate with InspectionWare applications that have communication over TCP Sockets enabled such as **IW Automate**.

Initializing communications over TCP socket.

_Setting up the socket_
```csharp
//Initialize the transport manager with the localhost ip and port
_socketTransportManager = new SocketTransportManager(IPAddress.Loopback, 885);

//Specify the event handler for when the socket is ready to start communicating
_socketTransportManager.SocketReady += OnSocketReady
_socketTransportManager.Connect(5000, ()=>
{
    //On Connection Timeout (5000ms in this case)
});
```

_Setting up server instance_
```csharp
if (_server != null)
    _server.Dispose();

_server = new IWServer(_socketTransportManager);
```

### Variables

Variables are the most common items that are transmitted across, they can enable full handshaking between the two applications.

To be able to transmit or receive a variable, it must first be registered with the `IWServer`.

_Set up an instance of_ `ServedString`
```csharp
_stringVariable = new ServedString(STRING_VARIABLE_NAME);
_stringVariable.PropertySet += (s, e) =>
{
    //Event for property changed
}
```
>NOTE: The `PropertySet` event is not invoked on the UI thread, to update ui controls when property is changed, make sure to do it on the proper thread.

_Register the_ `_stringVariable` with `IWServer`
```csharp
_server.RegisterServedProperty(_stringVariable);
```
Now you should be able to get and set values for the variable.

The value of the variable can be accessed using the `Value` property.

#### Getting and Setting Variables

Once a variable has been registered, its value can either be set on the IW application or updated from the IW application.

##### Gets
Most gets are automatic, this means that when the value is changed in IW, the new value will be transmitted across and set to the served variable.
A get can be forced (e.g. on application launch to synchronize the values) using the `GetAllProperties()` method on an instance of `IWServer`.

##### Sets
There are two ways to set values to the IW application.

  1. Setting the `AutoSendWhenChanged` on the `IWServer` instance to `true` enables immediate transmission of a value when changed.
  2. Calling the `SetAllBufferedProperties()` or `SetAllProperties()` method on the `IWServer` instance sends values only if changed or force sends all values respectively.

## License

Copyright (C) 2020 Utex Scientific Instruments

This repository can not be copied and/or distributed without the express permission of [Utex Scientific Instruments](https://www.utex.com).

For more information please contact <tech-support@utex.com>
