﻿using System;
using System.Collections.Generic;
using System.Linq;
using UtexScientific.IWBridge.ServedObjects;
using UtexScientific.IWBridge.Transport;

namespace UtexScientific.IWBridge.Serialization
{
    internal class IWMessageHandler
    {
        private const string GET_MSG_START = "{Get ";
        private const string SET_MSG_START = "{Set ";

        public enum IWMessageType
        {
            SET_PROPERTY,
            GET_PROPERTY,
            EXECUTE_TASK,
            UNKNOWN
        }

        private TransportManager _transportManager;

        public IWMessageHandler(TransportManager transportManager)
        {
            _transportManager = transportManager;
        }

        public IWMessageType GetMessageType(string message)
        {
            if (message.StartsWith(GET_MSG_START))
            {
                return IWMessageType.GET_PROPERTY;
            }
            else if (message.StartsWith(SET_MSG_START))
            {
                return IWMessageType.SET_PROPERTY;
            }
            else
            {
                return IWMessageType.UNKNOWN;
            }
        }

        public List<(string propertyName, string value)> ParseSetMessage(string message)
        {
            var varsToSet = new List<(string, string)>();

            message = message.Replace(SET_MSG_START, "");
            message = message.Replace("}", "");

            var messageParams = message.Split(',').ToList();
            for (int i = messageParams.Count - 1; i >= 0; i--)
            {
                messageParams[i] = messageParams[i].Trim();

                if (messageParams[i].StartsWith("!") ||
                    messageParams[i].StartsWith("?") ||
                    messageParams[i].StartsWith("/"))
                    messageParams.RemoveAt(i);
            }

            foreach (var property in messageParams)
            {
                try
                {
                    var propName = property.Split('=')[0].Trim();
                    var propVal = property.Split('=')[1].Trim();

                    varsToSet.Add((propName, propVal));
                }
                catch (Exception)
                {
                    continue;
                }
            }

            return varsToSet;
        }

        public List<string> ParseGetMessage(string message)
        {
            var propsToGet = new List<string>();

            message = message.Replace(GET_MSG_START, "");
            message = message.Replace("}", "");

            var messageParams = message.Split(',').ToList();
            for (int i = messageParams.Count - 1; i >= 0; i--)
            {
                messageParams[i] = messageParams[i].Trim();

                if (messageParams[i].StartsWith("!") ||
                    messageParams[i].StartsWith("?") ||
                    messageParams[i].StartsWith("/"))
                    messageParams.RemoveAt(i);
            }

            foreach (var property in messageParams)
            {
                propsToGet.Add(property);
            }

            return propsToGet;
        }

        public void SetProperty(IServable property)
        {
            SetProperties(new List<IServable>() { property });
        }

        public void SetProperties(IEnumerable<IServable> properties)
        {
            if (properties == null || properties.Count() == 0)
                return;

            properties.ToList().ForEach(prop =>
            {
                if (prop is IBufferable)
                    (prop as IBufferable).PushBuffer();
            });

            var setPropertiesMessage = BuildSetPropertyMessage(properties);

            _transportManager.SendMessage(setPropertiesMessage);

            //GetProperties(properties);
        }

        public void GetProperty(IServable property)
        {
            GetProperties(new List<IServable>() { property });
        }

        public void GetProperties(IEnumerable<IServable> properties)
        {
            if (properties == null || properties.Count() == 0)
                return;

            var getPropertiesMessage = BuildGetPropertyMessage(properties);

            _transportManager.SendMessage(getPropertiesMessage);
        }

        private string BuildSetPropertyMessage(IEnumerable<IServable> properties)
        {
            var message = SET_MSG_START;

            foreach (var property in properties)
            {
                var propName = property.GetPropertyName();
                var value = property.GetStringValue();

                if (string.IsNullOrEmpty(propName))
                    continue;

                if (string.IsNullOrEmpty(value))
                {
                    message += $"{propName},";
                }
                else
                {
                    message += $"{propName} = {value},";
                }
            }

            message += @" /nr";
            message += @"}";

            return message;
        }

        private string BuildGetPropertyMessage(IEnumerable<IServable> properties)
        {
            var message = GET_MSG_START;

            foreach (var property in properties)
            {
                var propName = property.GetPropertyName();

                message += $"{propName}, ";
            }

            message += @" /nr";
            message += @"}";

            return message;
        }
    }
}