﻿using System;

namespace UtexScientific.IWBridge.Transport
{
    public class MessageReceiveEventArgs : EventArgs
    {
        public string Message { get; private set; }

        public MessageReceiveEventArgs(string message)
        {
            Message = message;
        }
    }
}