﻿using System;

namespace UtexScientific.IWBridge.Transport
{
    public abstract class TransportManager
    {
        public abstract void Dispose();

        public abstract void SendMessage(string message);

        public abstract event EventHandler<MessageReceiveEventArgs> OnMessageReceived;

        public abstract event EventHandler TransporterConnected;

        public abstract event EventHandler TransporterDisposed;
    }
}