﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace UtexScientific.IWBridge.Transport
{
    public class SocketTransportManager : TransportManager
    {
        /// <summary>
        /// Event is fired when a new message is received through the socket.
        /// Use this if access to the raw message body is needed.
        /// Event is fired on a secondary thread.
        /// </summary>
        public override event EventHandler<MessageReceiveEventArgs> OnMessageReceived;

        /// <summary>
        /// Event is fired when the socket is connected and the listener thread is active.
        /// All SendMessage calls must happen after this event
        /// </summary>
        public event EventHandler SocketReady;

        /// <summary>
        /// When the socket is disposed, either by calling Dispose() or due to heartbeat timeout.
        /// </summary>
        public override event EventHandler TransporterDisposed;

        public override event EventHandler TransporterConnected;

        /// <summary>
        /// IP Address of the destination
        /// </summary>
        public IPAddress Address { get; private set; }

        /// <summary>
        /// Connection port of the destination
        /// </summary>
        public int Port { get; private set; }

        /// <summary>
        /// Flag used to check if the socket is active
        /// </summary>
        public bool IsRunning { get; private set; }

        private ManualResetEvent _connectionTimeout;
        private TcpClient _client;
        private Thread _messageListenerThread;
        private NetworkStream _networkStream;

        public SocketTransportManager(IPAddress address, int port)
        {
            Address = address;
            Port = port;

            _client = new TcpClient();
            _connectionTimeout = new ManualResetEvent(false);
        }

        /// <summary>
        /// Connects the socket client to the listener running in InspectionWare
        /// </summary>
        /// <param name="timeoutMs">Connection attempt timeout in ms</param>
        /// <param name="onConnectionTimeout">Action executed when a timeout occurs</param>
        /// <returns></returns>
        public bool Connect(int timeoutMs, Action onConnectionTimeout)
        {
            if (_client == null)
                return false;

            _connectionTimeout.Reset();

            _client.BeginConnect(Address, Port, asyncCallback =>
            {
                _connectionTimeout.Set();
            }, _client);

            if (_connectionTimeout.WaitOne(timeoutMs, false))
            {
                if (_client.Connected)
                {
                    TransporterConnected?.Invoke(this, EventArgs.Empty);
                    StartListening();
                    return true;
                }
                else
                {
                    IsRunning = false;
                    return false;
                }
            }
            else
            {
                Dispose();
                onConnectionTimeout();
            }

            return false;
        }

        /// <summary>
        /// Send a message over the socket.
        /// The message is encoded as an ASCII string, so Unicode characters will cause errors
        /// </summary>
        /// <param name="message"></param>
        public override void SendMessage(string message)
        {
            var payload = Encoding.ASCII.GetBytes(message);

            if (_client.Connected && IsRunning)
                _networkStream.Write(payload, 0, payload.Length);
        }

        private void StartListening()
        {
            _networkStream = _client.GetStream();
            _messageListenerThread = new Thread(MessageListener);
            _messageListenerThread.Start();

            IsRunning = true;

            SocketReady?.Invoke(this, EventArgs.Empty);
        }

        private void MessageListener()
        {
            while (IsRunning)
            {
                try
                {
                    if (_client.ReceiveBufferSize > 0 && _client.Available > 0)
                    {
                        var buffer = new byte[_client.ReceiveBufferSize];
                        _networkStream.Read(buffer, 0, buffer.Length);

                        var message = Encoding.ASCII.GetString(buffer, 0, buffer.TakeWhile(b => b != 0).Count());

                        OnMessageReceived?.Invoke(this, new MessageReceiveEventArgs(message));
                    }
                }
                catch (Exception e)
                {
                    Debug.WriteLine(e.Message);
                }
            }
        }

        /// <summary>
        /// Closes the socket connection and stops the message listener thread
        /// </summary>
        public override void Dispose()
        {
            if (IsRunning)
            {
                _client.Close();
                _client.Dispose();
                _messageListenerThread.Abort();
            }

            IsRunning = false;

            TransporterDisposed?.Invoke(this, EventArgs.Empty);

            TransporterDisposed = null;
            SocketReady = null;
            TransporterConnected = null;
        }
    }
}