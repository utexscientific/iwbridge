﻿using System;
using System.Collections.Generic;
using System.Linq;
using UtexScientific.IWBridge.Serialization;
using UtexScientific.IWBridge.ServedObjects;
using UtexScientific.IWBridge.Transport;

namespace UtexScientific.IWBridge
{
    public class IWServer : IDisposable
    {
        public Dictionary<string, ServedString> ServedStrings;
        public Dictionary<string, ServedBool> ServedBools;

        private Dictionary<string, IServable> RegisteredProperties;

        public bool AutoSendWhenChanged { get; set; }

        private TransportManager _transportManager;
        private IWMessageHandler _messageHandler;

        public IWServer(TransportManager transportManager)
        {
            _transportManager = transportManager;
            transportManager.OnMessageReceived += OnMessageReceived;

            ServedStrings = new Dictionary<string, ServedString>();
            ServedBools = new Dictionary<string, ServedBool>();

            RegisteredProperties = new Dictionary<string, IServable>();

            _messageHandler = new IWMessageHandler(_transportManager);
        }

        public void GetAllProperties()
        {
            _messageHandler.GetProperties(RegisteredProperties.Values);
        }

        public void SetAllProperties()
        {
            _messageHandler.SetProperties(RegisteredProperties.Values);
        }

        public void SetAllBufferedProperties()
        {
            _messageHandler.SetProperties(GetAllBufferedProperties());
        }

        private void OnMessageReceived(object sender, MessageReceiveEventArgs eventArgs)
        {
            if (eventArgs == null || string.IsNullOrEmpty(eventArgs.Message))
                return;

            var messageType = _messageHandler.GetMessageType(eventArgs.Message);
            switch (messageType)
            {
                case IWMessageHandler.IWMessageType.SET_PROPERTY:

                    var propsToSet = _messageHandler.ParseSetMessage(eventArgs.Message);
                    foreach (var property in propsToSet)
                    {
                        RegisteredProperties.TryGetValue(property.propertyName, out var registeredProperty);
                        if (registeredProperty != null)
                            registeredProperty.ParseString(property.value);
                    }

                    break;

                case IWMessageHandler.IWMessageType.GET_PROPERTY:

                    var responseProps = new List<IServable>();
                    var propsToGet = _messageHandler.ParseGetMessage(eventArgs.Message);
                    foreach (var property in propsToGet)
                    {
                        RegisteredProperties.TryGetValue(property, out var registeredProperty);
                        if (registeredProperty != null)
                            responseProps.Add(registeredProperty);
                    }

                    _messageHandler.SetProperties(responseProps);

                    break;
            }
        }

        public void RegisterServedProperty(IServable property)
        {
            if (property == null)
                return;

            if (string.IsNullOrEmpty(property.GetPropertyName()))
                return;

            RegisteredProperties?.Add(property.GetPropertyName(), property);
            if (property is IBufferable)
            {
                (property as IBufferable).BufferSet += (sender, args) =>
                {
                    if (AutoSendWhenChanged)
                        SetAllBufferedProperties();
                };
            }
        }

        public void UnregisterServedProperty(string propertyName)
        {
            RegisteredProperties.TryGetValue(propertyName, out var property);
            if (property != null)
                UnregisterServedProperty(property);
        }

        public void UnregisterServedProperty(IServable property)
        {
            RegisteredProperties?.Remove(property.GetPropertyName());
            if (property is IDisposable)
                (property as IDisposable).Dispose();
        }

        private List<IServable> GetAllBufferedProperties()
        {
            return RegisteredProperties.Values.Where(prop =>
            {
                if (prop is IBufferable)
                    return (prop as IBufferable).IsBuffering;

                return false;
            }).ToList();
        }

        public void Dispose()
        {
            _transportManager?.Dispose();
            _transportManager = null;

            foreach (var servedString in ServedStrings)
            {
                servedString.Value?.Dispose();
            }
        }
    }
}