﻿using System;

namespace UtexScientific.IWBridge.ServedObjects
{
    public interface IBufferable
    {
        bool IsBuffering { get; }

        void PushBuffer();

        void ClearBuffer();

        event EventHandler BufferSet;
    }

    public interface IBufferable<T> : IBufferable
    {
        T Value { get; }
        T BufferedValue { get; set; }
    }
}