﻿using System;

namespace UtexScientific.IWBridge.ServedObjects
{
    public class ServedFloat : IServable, IBufferable<float>, IDisposable
    {
        public float Value { get; private set; }

        public float BufferedValue
        {
            get { return _bufferedValue; }
            set
            {
                _bufferedValue = value;
                IsBuffering = true;
                BufferSet?.Invoke(this, EventArgs.Empty);
            }
        }

        public bool IsBuffering { get; private set; }

        public string PropertyName { get; private set; }

        public string Scope { get; set; }

        private float _bufferedValue;

        public event EventHandler BufferSet;

        public event EventHandler<PropertySetEventArgs> PropertySet;

        public ServedFloat(string propertyName)
        {
            PropertyName = propertyName;
        }

        public void ClearBuffer()
        {
            BufferedValue = 0f;
            IsBuffering = false;
        }

        public void Dispose()
        {
            PropertySet = null;
            BufferSet = null;
        }

        public string GetPropertyName()
        {
            return PropertyName;
        }

        public string GetScope()
        {
            return Scope;
        }

        public string GetStringValue()
        {
            return Convert.ToString(Value);
        }

        public void ParseString(string value)
        {
            try
            {
                var previousValue = Value;
                Value = float.Parse(value);
                PropertySet?.Invoke(this, new PropertySetEventArgs(previousValue));
            }
            catch (Exception) { }
        }

        public void PushBuffer()
        {
            if (IsBuffering)
                Value = BufferedValue;

            IsBuffering = false;
        }
    }
}