﻿using System;

namespace UtexScientific.IWBridge.ServedObjects
{
    internal class BufferSetEventArgs<T> : EventArgs
    {
        public T BufferedValue { get; private set; }

        public BufferSetEventArgs(T bufferedValue)
        {
            BufferedValue = bufferedValue;
        }
    }
}