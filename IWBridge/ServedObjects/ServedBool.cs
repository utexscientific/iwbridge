﻿using System;

namespace UtexScientific.IWBridge.ServedObjects
{
    public class ServedBool : IServable, IBufferable<bool>, IDisposable
    {
        public bool Value { get; private set; }

        public bool BufferedValue
        {
            get { return _bufferedValue; }
            set
            {
                _bufferedValue = value;
                IsBuffering = true;
                BufferSet?.Invoke(this, EventArgs.Empty);
            }
        }

        public bool IsBuffering { get; private set; }

        public string PropertyName { get; private set; }

        public string Scope { get; set; }

        private bool _bufferedValue;

        public event EventHandler BufferSet;

        public event EventHandler<PropertySetEventArgs> PropertySet;

        public ServedBool(string propertyName)
        {
            PropertyName = propertyName;
        }

        public void ClearBuffer()
        {
            BufferedValue = false;
            IsBuffering = false;
        }

        public void Dispose()
        {
            PropertySet = null;
            BufferSet = null;
        }

        public string GetPropertyName()
        {
            return PropertyName;
        }

        public string GetScope()
        {
            return Scope;
        }

        public string GetStringValue()
        {
            return Convert.ToString(Value);
        }

        public void ParseString(string value)
        {
            try
            {
                var previousValue = Value;
                Value = Convert.ToBoolean(value);
                PropertySet?.Invoke(this, new PropertySetEventArgs(previousValue));
            }
            catch (Exception) { }
        }

        public void PushBuffer()
        {
            if (IsBuffering)
                Value = BufferedValue;

            IsBuffering = false;
        }
    }
}