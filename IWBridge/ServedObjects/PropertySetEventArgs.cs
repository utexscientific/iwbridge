﻿using System;

namespace UtexScientific.IWBridge.ServedObjects
{
    public class PropertySetEventArgs : EventArgs
    {
        public object PreviousValue { get; private set; }

        public PropertySetEventArgs(object previousValue)
        {
            PreviousValue = previousValue;
        }
    }
}