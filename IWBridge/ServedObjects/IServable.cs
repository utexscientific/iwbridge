﻿using System;

namespace UtexScientific.IWBridge.ServedObjects
{
    public interface IServable
    {
        string GetPropertyName();

        string GetScope();

        string GetStringValue();

        void ParseString(string value);

        event EventHandler<PropertySetEventArgs> PropertySet;
    }
}