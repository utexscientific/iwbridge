﻿using System;

namespace UtexScientific.IWBridge.ServedObjects
{
    public class ServedString : IServable, IBufferable<string>, IDisposable
    {
        public string Value { get; private set; }

        public string BufferedValue
        {
            get { return _bufferedValue; }
            set
            {
                _bufferedValue = value;
                IsBuffering = true;
                BufferSet?.Invoke(this, EventArgs.Empty);
            }
        }

        public bool IsBuffering { get; private set; }

        public string PropertyName { get; private set; }

        public string Scope { get; set; }

        private string _bufferedValue;

        public event EventHandler BufferSet;

        public event EventHandler<PropertySetEventArgs> PropertySet;

        public ServedString(string propertyName)
        {
            PropertyName = propertyName;
        }

        public string GetPropertyName()
        {
            return PropertyName;
        }

        public string GetStringValue()
        {
            return Value;
        }

        public void ParseString(string value)
        {
            var previousValue = Value;
            Value = value;
            PropertySet?.Invoke(this, new PropertySetEventArgs(previousValue));
        }

        public void PushBuffer()
        {
            if (IsBuffering)
                Value = BufferedValue;

            IsBuffering = false;
        }

        public void ClearBuffer()
        {
            BufferedValue = string.Empty;
            IsBuffering = false;
        }

        public void Dispose()
        {
            PropertySet = null;
            BufferSet = null;
        }

        public string GetScope()
        {
            return Scope;
        }
    }
}