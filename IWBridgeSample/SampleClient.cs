﻿using System;
using System.Drawing;
using System.Net;
using System.Windows.Forms;
using UtexScientific.IWBridge;
using UtexScientific.IWBridge.ServedObjects;
using UtexScientific.IWBridge.Transport;

namespace IWBridgeSample
{
    public partial class SampleClient : Form
    {
        private IWServer _server;
        private SocketTransportManager _transportManager;

        private ServedString _stringVar;
        private ServedInt _intVar;
        private ServedDouble _doubleVar;
        private ServedFloat _floatVar;
        private ServedBool _boolVar;

        public SampleClient()
        {
            InitializeComponent();

            ipAddressTextBox.KeyPress += IPAddressKeyPressHandler;
            portTextBox.KeyPress += UnsignedIntegerKeyPressHandler;
            intVarTextBox.KeyPress += IntegerKeyPressHandler;
            doubleVarTextBox.KeyPress += RealKeyPressHandler;
            floatVarTextBox.KeyPress += RealKeyPressHandler;

            connectButton.Click += OnConnectPressed;
            disconnectButton.Click += OnDisconnectPressed;
            sendOnChangedCheckBox.CheckedChanged += OnAutoSetWhenChangedSet;
            setAllButton.Click += OnSetAllPressed;
            setBufferedButton.Click += OnSetBufferedPressed;
            getButton.Click += OnGetPressed;

            stringVarTextBox.TextChanged += OnStringVarChanged;
            intVarTextBox.TextChanged += OnIntVarChanged;
            doubleVarTextBox.TextChanged += OnDoubleVarChanged;
            floatVarTextBox.TextChanged += OnFloatVarChanged;
            boolVarCheckBox.CheckedChanged += OnBoolVarChanged;

            connectionStatusLabel.Text = "Disconnected";
            connectionStatusLabel.ForeColor = Color.Red;

            ipAddressTextBox.Text = "127.0.0.1";
            portTextBox.Text = "322";

            DisableVariableControls();
        }

        private void IPAddressKeyPressHandler(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
                e.Handled = true;
        }

        private void IntegerKeyPressHandler(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != '-')
                e.Handled = true;

            if (e.KeyChar == '-' && (sender as TextBox).SelectionStart != 0)
                e.Handled = true;
        }

        private void UnsignedIntegerKeyPressHandler(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
                e.Handled = true;
        }

        private void RealKeyPressHandler(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != '-' && e.KeyChar != '.')
                e.Handled = true;

            if (e.KeyChar == '-' && (sender as TextBox).SelectionStart != 0)
                e.Handled = true;

            if (e.KeyChar == '.' && (sender as TextBox).Text.IndexOf('.') != -1)
                e.Handled = true;
        }

        private void OnConnectPressed(object sender, EventArgs e)
        {
            IPAddress ip = null;
            try
            {
                ip = IPAddress.Parse(ipAddressTextBox.Text);
            }
            catch (FormatException)
            {
                MessageBox.Show("Invalid IP Address");
            }

            if (ip == null)
                return;

            var port = int.Parse(portTextBox.Text);

            _transportManager = new SocketTransportManager(ip, port);
            _transportManager.SocketReady += (snder, eargs) =>
            {
                connectionStatusLabel.Text = "Connected";
                connectionStatusLabel.ForeColor = Color.Green;
                EnableConnectionButtons();
                EnableVariableControls();
                connectButton.Enabled = false;

                InitializeVariables();
            };

            connectionStatusLabel.Text = "Connecting";
            connectionStatusLabel.ForeColor = Color.Orange;
            DisableConnectionButtons();

            _transportManager.Connect(5000, () =>
            {
                connectionStatusLabel.Text = "Timeout";
                connectionStatusLabel.ForeColor = Color.Red;
                EnableConnectionButtons();
                DisableVariableControls();
            });
        }

        private void OnDisconnectPressed(object sender, EventArgs e)
        {
            EnableConnectionButtons();
            DisableVariableControls();

            connectionStatusLabel.Text = "Disconnected";
            connectionStatusLabel.ForeColor = Color.Red;

            _server?.Dispose();
            _server = null;
        }

        private void OnAutoSetWhenChangedSet(object sender, EventArgs e)
        {
            if (_server != null)
                _server.AutoSendWhenChanged = sendOnChangedCheckBox.Checked;
        }

        private void OnStringVarChanged(object sender, EventArgs e)
        {
            if (_stringVar != null)
                _stringVar.BufferedValue = stringVarTextBox.Text;
        }

        private void OnIntVarChanged(object sender, EventArgs e)
        {
            if (_intVar != null)
            {
                try
                {
                    _intVar.BufferedValue = int.Parse(intVarTextBox.Text);
                }
                catch (Exception)
                {
                    _intVar.BufferedValue = 0;
                }
            }
        }

        private void OnDoubleVarChanged(object sender, EventArgs e)
        {
            if (_doubleVar != null)
            {
                try
                {
                    _doubleVar.BufferedValue = double.Parse(doubleVarTextBox.Text);
                }
                catch (Exception)
                {
                    _doubleVar.BufferedValue = 0d;
                }
            }
        }

        private void OnFloatVarChanged(object sender, EventArgs e)
        {
            if (_floatVar != null)
            {
                try
                {
                    _floatVar.BufferedValue = float.Parse(floatVarTextBox.Text);
                }
                catch (Exception)
                {
                    _floatVar.BufferedValue = 0f;
                }
            }
        }

        private void OnBoolVarChanged(object sender, EventArgs e)
        {
            if (_boolVar != null)
                _boolVar.BufferedValue = boolVarCheckBox.Checked;
        }

        private void OnSetBufferedPressed(object sender, EventArgs e)
        {
            _server?.SetAllBufferedProperties();
        }

        private void OnSetAllPressed(object sender, EventArgs e)
        {
            _server?.SetAllProperties();
        }

        private void OnGetPressed(object sender, EventArgs e)
        {
            _server?.GetAllProperties();
        }

        private void InitializeVariables()
        {
            if (_server != null)
                _server.Dispose();

            _server = new IWServer(_transportManager);

            _stringVar = new ServedString("String Var");
            _stringVar.PropertySet += (s, e) =>
            {
                stringVarTextBox.Invoke((MethodInvoker)delegate
                {
                    stringVarTextBox.Text = _stringVar.Value;
                });
            };

            _intVar = new ServedInt("Int Var");
            _intVar.PropertySet += (s, e) =>
            {
                intVarTextBox.Invoke((MethodInvoker)delegate
               {
                   intVarTextBox.Text = _intVar.Value.ToString();
               });
            };

            _doubleVar = new ServedDouble("Double Var");
            _doubleVar.PropertySet += (s, e) =>
            {
                doubleVarTextBox.Invoke((MethodInvoker)delegate
                {
                    doubleVarTextBox.Text = _doubleVar.Value.ToString();
                });
            };

            _floatVar = new ServedFloat("Float Var");
            _floatVar.PropertySet += (s, e) =>
            {
                floatVarTextBox.Invoke((MethodInvoker)delegate
                {
                    floatVarTextBox.Text = _floatVar.Value.ToString();
                });
            };

            _boolVar = new ServedBool("Bool Var");
            _boolVar.PropertySet += (s, e) =>
            {
                boolVarCheckBox.Invoke((MethodInvoker)delegate
                {
                    boolVarCheckBox.Checked = _boolVar.Value;
                });
            };

            _server.RegisterServedProperty(_stringVar);
            _server.RegisterServedProperty(_intVar);
            _server.RegisterServedProperty(_doubleVar);
            _server.RegisterServedProperty(_floatVar);
            _server.RegisterServedProperty(_boolVar);
        }

        private void OnClosing(object sender, FormClosingEventArgs e)
        {
            _server?.Dispose();
        }

        private void DisableConnectionButtons()
        {
            connectButton.Enabled = false;
            disconnectButton.Enabled = false;
        }

        private void EnableConnectionButtons()
        {
            connectButton.Enabled = true;
            disconnectButton.Enabled = true;
        }

        private void DisableVariableControls()
        {
            sendOnChangedCheckBox.Enabled = false;
            getButton.Enabled = false;
            setAllButton.Enabled = false;
            setBufferedButton.Enabled = false;
        }

        private void EnableVariableControls()
        {
            sendOnChangedCheckBox.Enabled = true;
            getButton.Enabled = true;
            setAllButton.Enabled = true;
            setBufferedButton.Enabled = true;
        }
    }
}