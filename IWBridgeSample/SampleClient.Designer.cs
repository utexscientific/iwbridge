﻿namespace IWBridgeSample
{
    partial class SampleClient
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.navigationTabControl = new System.Windows.Forms.TabControl();
            this.tcpClientTabPage = new System.Windows.Forms.TabPage();
            this.variablesGroup = new System.Windows.Forms.GroupBox();
            this.setBufferedButton = new System.Windows.Forms.Button();
            this.setAllButton = new System.Windows.Forms.Button();
            this.getButton = new System.Windows.Forms.Button();
            this.sendOnChangedCheckBox = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.boolVarCheckBox = new System.Windows.Forms.CheckBox();
            this.floatVarTextBox = new System.Windows.Forms.TextBox();
            this.doubleVarTextBox = new System.Windows.Forms.TextBox();
            this.intVarTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.stringVarTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.connectionGroup = new System.Windows.Forms.GroupBox();
            this.connectionStatusLabel = new System.Windows.Forms.Label();
            this.disconnectButton = new System.Windows.Forms.Button();
            this.connectButton = new System.Windows.Forms.Button();
            this.portTextBox = new System.Windows.Forms.TextBox();
            this.portLabel = new System.Windows.Forms.Label();
            this.ipAddressLabel = new System.Windows.Forms.Label();
            this.ipAddressTextBox = new System.Windows.Forms.TextBox();
            this.navigationTabControl.SuspendLayout();
            this.tcpClientTabPage.SuspendLayout();
            this.variablesGroup.SuspendLayout();
            this.connectionGroup.SuspendLayout();
            this.SuspendLayout();
            // 
            // navigationTabControl
            // 
            this.navigationTabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.navigationTabControl.Controls.Add(this.tcpClientTabPage);
            this.navigationTabControl.Location = new System.Drawing.Point(0, 0);
            this.navigationTabControl.Margin = new System.Windows.Forms.Padding(6);
            this.navigationTabControl.Name = "navigationTabControl";
            this.navigationTabControl.SelectedIndex = 0;
            this.navigationTabControl.Size = new System.Drawing.Size(488, 362);
            this.navigationTabControl.TabIndex = 0;
            // 
            // tcpClientTabPage
            // 
            this.tcpClientTabPage.Controls.Add(this.variablesGroup);
            this.tcpClientTabPage.Controls.Add(this.connectionGroup);
            this.tcpClientTabPage.Location = new System.Drawing.Point(4, 22);
            this.tcpClientTabPage.Margin = new System.Windows.Forms.Padding(0);
            this.tcpClientTabPage.Name = "tcpClientTabPage";
            this.tcpClientTabPage.Padding = new System.Windows.Forms.Padding(8);
            this.tcpClientTabPage.Size = new System.Drawing.Size(480, 336);
            this.tcpClientTabPage.TabIndex = 0;
            this.tcpClientTabPage.Text = "TCP Client";
            this.tcpClientTabPage.UseVisualStyleBackColor = true;
            // 
            // variablesGroup
            // 
            this.variablesGroup.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.variablesGroup.Controls.Add(this.setBufferedButton);
            this.variablesGroup.Controls.Add(this.setAllButton);
            this.variablesGroup.Controls.Add(this.getButton);
            this.variablesGroup.Controls.Add(this.sendOnChangedCheckBox);
            this.variablesGroup.Controls.Add(this.label5);
            this.variablesGroup.Controls.Add(this.label4);
            this.variablesGroup.Controls.Add(this.label3);
            this.variablesGroup.Controls.Add(this.boolVarCheckBox);
            this.variablesGroup.Controls.Add(this.floatVarTextBox);
            this.variablesGroup.Controls.Add(this.doubleVarTextBox);
            this.variablesGroup.Controls.Add(this.intVarTextBox);
            this.variablesGroup.Controls.Add(this.label2);
            this.variablesGroup.Controls.Add(this.stringVarTextBox);
            this.variablesGroup.Controls.Add(this.label1);
            this.variablesGroup.Location = new System.Drawing.Point(8, 83);
            this.variablesGroup.Name = "variablesGroup";
            this.variablesGroup.Size = new System.Drawing.Size(461, 242);
            this.variablesGroup.TabIndex = 1;
            this.variablesGroup.TabStop = false;
            this.variablesGroup.Text = "Variables";
            // 
            // setBufferedButton
            // 
            this.setBufferedButton.Location = new System.Drawing.Point(171, 187);
            this.setBufferedButton.Name = "setBufferedButton";
            this.setBufferedButton.Size = new System.Drawing.Size(88, 23);
            this.setBufferedButton.TabIndex = 13;
            this.setBufferedButton.Text = "Set Changed";
            this.setBufferedButton.UseVisualStyleBackColor = true;
            // 
            // setAllButton
            // 
            this.setAllButton.Location = new System.Drawing.Point(90, 187);
            this.setAllButton.Name = "setAllButton";
            this.setAllButton.Size = new System.Drawing.Size(75, 23);
            this.setAllButton.TabIndex = 12;
            this.setAllButton.Text = "Set All";
            this.setAllButton.UseVisualStyleBackColor = true;
            // 
            // getButton
            // 
            this.getButton.Location = new System.Drawing.Point(9, 187);
            this.getButton.Name = "getButton";
            this.getButton.Size = new System.Drawing.Size(75, 23);
            this.getButton.TabIndex = 11;
            this.getButton.Text = "Get";
            this.getButton.UseVisualStyleBackColor = true;
            // 
            // sendOnChangedCheckBox
            // 
            this.sendOnChangedCheckBox.AutoSize = true;
            this.sendOnChangedCheckBox.Location = new System.Drawing.Point(9, 163);
            this.sendOnChangedCheckBox.Name = "sendOnChangedCheckBox";
            this.sendOnChangedCheckBox.Size = new System.Drawing.Size(130, 17);
            this.sendOnChangedCheckBox.TabIndex = 10;
            this.sendOnChangedCheckBox.Text = "Auto Set On Changed";
            this.sendOnChangedCheckBox.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 125);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Bool Var";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 102);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Float Var";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 76);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Double Var";
            // 
            // boolVarCheckBox
            // 
            this.boolVarCheckBox.AutoSize = true;
            this.boolVarCheckBox.Location = new System.Drawing.Point(72, 125);
            this.boolVarCheckBox.Name = "boolVarCheckBox";
            this.boolVarCheckBox.Size = new System.Drawing.Size(15, 14);
            this.boolVarCheckBox.TabIndex = 6;
            this.boolVarCheckBox.UseVisualStyleBackColor = true;
            // 
            // floatVarTextBox
            // 
            this.floatVarTextBox.Location = new System.Drawing.Point(72, 99);
            this.floatVarTextBox.Name = "floatVarTextBox";
            this.floatVarTextBox.Size = new System.Drawing.Size(100, 20);
            this.floatVarTextBox.TabIndex = 5;
            // 
            // doubleVarTextBox
            // 
            this.doubleVarTextBox.Location = new System.Drawing.Point(72, 73);
            this.doubleVarTextBox.Name = "doubleVarTextBox";
            this.doubleVarTextBox.Size = new System.Drawing.Size(100, 20);
            this.doubleVarTextBox.TabIndex = 4;
            // 
            // intVarTextBox
            // 
            this.intVarTextBox.Location = new System.Drawing.Point(72, 46);
            this.intVarTextBox.Name = "intVarTextBox";
            this.intVarTextBox.Size = new System.Drawing.Size(100, 20);
            this.intVarTextBox.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Int Var";
            // 
            // stringVarTextBox
            // 
            this.stringVarTextBox.Location = new System.Drawing.Point(72, 19);
            this.stringVarTextBox.Name = "stringVarTextBox";
            this.stringVarTextBox.Size = new System.Drawing.Size(158, 20);
            this.stringVarTextBox.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "String Var";
            // 
            // connectionGroup
            // 
            this.connectionGroup.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.connectionGroup.Controls.Add(this.connectionStatusLabel);
            this.connectionGroup.Controls.Add(this.disconnectButton);
            this.connectionGroup.Controls.Add(this.connectButton);
            this.connectionGroup.Controls.Add(this.portTextBox);
            this.connectionGroup.Controls.Add(this.portLabel);
            this.connectionGroup.Controls.Add(this.ipAddressLabel);
            this.connectionGroup.Controls.Add(this.ipAddressTextBox);
            this.connectionGroup.Location = new System.Drawing.Point(8, 11);
            this.connectionGroup.Name = "connectionGroup";
            this.connectionGroup.Size = new System.Drawing.Size(460, 66);
            this.connectionGroup.TabIndex = 0;
            this.connectionGroup.TabStop = false;
            this.connectionGroup.Text = "Connection";
            // 
            // connectionStatusLabel
            // 
            this.connectionStatusLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.connectionStatusLabel.AutoSize = true;
            this.connectionStatusLabel.ForeColor = System.Drawing.Color.Red;
            this.connectionStatusLabel.Location = new System.Drawing.Point(250, 22);
            this.connectionStatusLabel.Name = "connectionStatusLabel";
            this.connectionStatusLabel.Size = new System.Drawing.Size(73, 13);
            this.connectionStatusLabel.TabIndex = 6;
            this.connectionStatusLabel.Text = "Disconnected";
            // 
            // disconnectButton
            // 
            this.disconnectButton.Location = new System.Drawing.Point(334, 35);
            this.disconnectButton.Name = "disconnectButton";
            this.disconnectButton.Size = new System.Drawing.Size(75, 22);
            this.disconnectButton.TabIndex = 5;
            this.disconnectButton.Text = "Disconnect";
            this.disconnectButton.UseVisualStyleBackColor = true;
            // 
            // connectButton
            // 
            this.connectButton.Location = new System.Drawing.Point(253, 35);
            this.connectButton.Name = "connectButton";
            this.connectButton.Size = new System.Drawing.Size(75, 22);
            this.connectButton.TabIndex = 4;
            this.connectButton.Text = "Connect";
            this.connectButton.UseVisualStyleBackColor = true;
            // 
            // portTextBox
            // 
            this.portTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.portTextBox.Location = new System.Drawing.Point(166, 38);
            this.portTextBox.Name = "portTextBox";
            this.portTextBox.Size = new System.Drawing.Size(81, 20);
            this.portTextBox.TabIndex = 3;
            // 
            // portLabel
            // 
            this.portLabel.AutoSize = true;
            this.portLabel.Location = new System.Drawing.Point(163, 22);
            this.portLabel.Name = "portLabel";
            this.portLabel.Size = new System.Drawing.Size(26, 13);
            this.portLabel.TabIndex = 2;
            this.portLabel.Text = "Port";
            // 
            // ipAddressLabel
            // 
            this.ipAddressLabel.AutoSize = true;
            this.ipAddressLabel.Location = new System.Drawing.Point(6, 22);
            this.ipAddressLabel.Name = "ipAddressLabel";
            this.ipAddressLabel.Size = new System.Drawing.Size(58, 13);
            this.ipAddressLabel.TabIndex = 1;
            this.ipAddressLabel.Text = "IP Address";
            // 
            // ipAddressTextBox
            // 
            this.ipAddressTextBox.Location = new System.Drawing.Point(9, 38);
            this.ipAddressTextBox.Name = "ipAddressTextBox";
            this.ipAddressTextBox.Size = new System.Drawing.Size(151, 20);
            this.ipAddressTextBox.TabIndex = 0;
            // 
            // SampleClient
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(484, 361);
            this.Controls.Add(this.navigationTabControl);
            this.MinimumSize = new System.Drawing.Size(500, 400);
            this.Name = "SampleClient";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "IWBridge Sample Application";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.OnClosing);
            this.navigationTabControl.ResumeLayout(false);
            this.tcpClientTabPage.ResumeLayout(false);
            this.variablesGroup.ResumeLayout(false);
            this.variablesGroup.PerformLayout();
            this.connectionGroup.ResumeLayout(false);
            this.connectionGroup.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl navigationTabControl;
        private System.Windows.Forms.TabPage tcpClientTabPage;
        private System.Windows.Forms.GroupBox connectionGroup;
        private System.Windows.Forms.TextBox portTextBox;
        private System.Windows.Forms.Label portLabel;
        private System.Windows.Forms.Label ipAddressLabel;
        private System.Windows.Forms.TextBox ipAddressTextBox;
        private System.Windows.Forms.Button disconnectButton;
        private System.Windows.Forms.Button connectButton;
        private System.Windows.Forms.GroupBox variablesGroup;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label connectionStatusLabel;
        private System.Windows.Forms.TextBox floatVarTextBox;
        private System.Windows.Forms.TextBox doubleVarTextBox;
        private System.Windows.Forms.TextBox intVarTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox stringVarTextBox;
        private System.Windows.Forms.CheckBox boolVarCheckBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox sendOnChangedCheckBox;
        private System.Windows.Forms.Button setAllButton;
        private System.Windows.Forms.Button getButton;
        private System.Windows.Forms.Button setBufferedButton;
    }
}

